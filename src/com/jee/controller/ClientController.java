package com.jee.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import  com.lb2.model.Client;

import com.lb2.dao.ClientDAO;

/**
 * Servlet implementation class ClientController
 */
@WebServlet("/ClientController")
public class ClientController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClientController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		
		if(action == null) {
			ClientDAO clientDAO = new ClientDAO();
			List<Client> clients = clientDAO.getAll();
			request.setAttribute("clients", clients);
			RequestDispatcher ds = request.getRequestDispatcher("client.jsp");
			ds.forward(request, response);
			return;
		}
		
		switch(action) {
		case "add":
			RequestDispatcher ds = request.getRequestDispatcher("addClient.jsp");
			ds.forward(request, response);
			break;
		case "delete":
			int idToDelete = Integer.parseInt(request.getParameter("id"));
			ClientDAO dao = new ClientDAO();
			dao.Delete(idToDelete);
			response.sendRedirect("http://localhost:8080/jee/ClientController");
			break;
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String fname = request.getParameter("fname");
		String sname = request.getParameter("sname");
		String lname = request.getParameter("lname");
		
		Client client = new Client();
		client.setFname(fname);
		client.setSname(sname);
		client.setLname(lname);
		
		ClientDAO clientDAO = new ClientDAO();
		clientDAO.Create(client);
		
		response.sendRedirect("http://localhost:8080/jee/ClientController");
	}

}
