package com.lb2.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.lb2.model.Client;

public class ClientDAO {
	public List<Client> getAll() {

		Connection connection = null;
		ArrayList<Client> clients = new ArrayList<>();

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionUrl = "jdbc:sqlserver://localhost;" + "database=bablo;" + "user=java_user;"
					+ "password=1111";
			connection = DriverManager.getConnection(connectionUrl);
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT * FROM CLIENT");

			while (rs.next()) {
				Client client = new Client();
				client.setId(rs.getInt("ID"));
				client.setFname(rs.getString("FNAME"));
				client.setSname(rs.getString("SNAME"));
				client.setLname(rs.getString("LNAME"));
				clients.add(client);
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return clients;
	}
	
	public void Create(Client client) {
		Connection connection = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionUrl = "jdbc:sqlserver://localhost;" + "database=bablo;" + "user=java_user;"
					+ "password=1111";
			connection = DriverManager.getConnection(connectionUrl);
			
			PreparedStatement statement = 
					connection.prepareStatement("INSERT INTO CLIENT VALUES(?,?,?)");
			statement.setString(1, client.getFname());
			statement.setString(2, client.getSname());
			statement.setString(3, client.getLname());
			statement.executeUpdate();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void Delete(int id) {
		Connection connection = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			String connectionUrl = "jdbc:sqlserver://localhost;" + "database=bablo;" + "user=java_user;"
					+ "password=1111";
			connection = DriverManager.getConnection(connectionUrl);
			
			PreparedStatement statement = 
					connection.prepareStatement("DELETE FROM CLIENT WHERE ID = ?");
			statement.setInt(1, id);
			statement.executeUpdate();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
