
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
 <%@
 	page import="java.util.*"
  %>
  
  <%@
  	page import="com.lb2.model.*"
   %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
 <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

</head>
<body>

<h1>Clients list</h1>

<hr>
<a href="?action=add">Add Client</a>
<table class="table table-striped">

<thead>
<th>ID</th>
<th>FNAME</th>
<th>Controls</th>
</thead>

<tbody>

<%

List<Client> clients = (List<Client>)request.getAttribute("clients");

for(Client client : clients){
	out.print("<tr>");
	
	out.print("<td>");
	out.print(client.getId());
	out.print("</td>");
	
	out.print("<td>");
	out.print(client.getFname());
	out.print("</td>");
	
	out.print("<td>");
	out.print("<div class=\"btn-group\" role=\"group\" aria-label=\"Basic example\">"+
			  "<a href=\"?action=delete&id="+client.getId()+"\"><button type=\"button\" class=\"btn btn-secondary\">Update</button></a>"+
			"<a href=\"\"><button type=\"button\" class=\"btn btn-secondary\">Delete</button></a>"+
			"</div>");
	out.print("</td>");
	
	out.print("</tr>");
}

%>

</tbody>

</table>

</body>
</html>